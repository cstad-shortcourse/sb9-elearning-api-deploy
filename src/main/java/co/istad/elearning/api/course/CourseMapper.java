package co.istad.elearning.api.course;

import co.istad.elearning.api.course.dto.CourseCreationDto;
import co.istad.elearning.api.course.dto.CourseDto;
import co.istad.elearning.api.course.dto.CourseEditionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourseMapper {

    CourseDto toCourseDto(Course course);

    List<CourseDto> toCourseListDto(List<Course> courses);

    @Mapping(source = "categoryId", target = "category.id")
    @Mapping(source = "instructorId", target = "instructor.id")
    Course fromCourseCreationDto(CourseCreationDto courseCreationDto);

    void fromCourseEditionDto(@MappingTarget Course course, CourseEditionDto courseEditionDto);

}
