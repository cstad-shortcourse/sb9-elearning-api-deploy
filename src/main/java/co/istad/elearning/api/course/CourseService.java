package co.istad.elearning.api.course;

import co.istad.elearning.api.course.dto.CourseCreationDto;
import co.istad.elearning.api.course.dto.CourseDto;
import co.istad.elearning.api.course.dto.CourseEditionDto;

import java.util.List;

public interface CourseService {

    List<CourseDto> findList();

    CourseDto findById(Long id);

    void createNew(CourseCreationDto courseCreationDto);

    void editById(Long id, CourseEditionDto courseEditionDto);

    void disableById(Long id);
}
