package co.istad.elearning.api.category.dto;

import lombok.Builder;

@Builder
public record CategoryDto(
        Integer id,
        String name
) {
}
