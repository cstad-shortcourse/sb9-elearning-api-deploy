package co.istad.elearning.api.auth.dto;

import lombok.Builder;

@Builder
public record AuthDto(
        String tokenType,
        String accessToken,
        String refreshToken
) {
}
