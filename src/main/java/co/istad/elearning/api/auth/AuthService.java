package co.istad.elearning.api.auth;

import co.istad.elearning.api.auth.dto.*;
import jakarta.mail.MessagingException;

import java.util.Map;

public interface AuthService {

    AuthDto refresh(RefreshTokenDto refreshTokenDto);

    AuthDto login(LoginDto loginDto);

    Map<String, Object> register(RegisterDto registerDto) throws MessagingException;

    Map<String, Object> verify(VerifyDto verifyDto);
}
