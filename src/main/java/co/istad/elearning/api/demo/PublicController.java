package co.istad.elearning.api.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/public")
public class PublicController {

    @GetMapping
    Map<String, Object> index() {
        return Map.of(
                "message", "Welcome to public API"
        );
    }

}
