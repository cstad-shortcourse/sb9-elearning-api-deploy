package co.istad.elearning.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ELearningApi {

    public static void main(String[] args) {
        SpringApplication.run(ELearningApi.class, args);
    }

}
