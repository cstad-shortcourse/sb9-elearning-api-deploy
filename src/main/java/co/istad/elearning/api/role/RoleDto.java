package co.istad.elearning.api.role;

import java.util.List;

public record RoleDto(
        String name,
        List<String> authorities
) {
}
