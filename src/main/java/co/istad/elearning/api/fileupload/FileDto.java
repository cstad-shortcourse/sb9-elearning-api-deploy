package co.istad.elearning.api.fileupload;

import lombok.Builder;

@Builder
public record FileDto(
        String name,
        String extension,
        Long size,
        String uri
) {
}
