package co.istad.elearning.api.user;

import co.istad.elearning.api.role.Role;
import co.istad.elearning.api.role.RoleRepository;
import co.istad.elearning.api.user.dto.UserCreationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public void createNew(UserCreationDto userCreationDto) {

        User user = userMapper.fromUserCreationDto(userCreationDto);
        user.setIsDeleted(false);
        user.setIsVerified(false);

        // Encode password before save
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (userRepository.existsByEmail(user.getEmail())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Email is already existed!");
        }

        if (userRepository.existsByUsername(user.getUsername())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Username is already existed!");
        }

        Set<Role> roles = new HashSet<>();
        userCreationDto.roleNames().forEach(name -> {
            Role role = roleRepository.findByName(name)
                    .orElseThrow(() -> new ResponseStatusException(
                            HttpStatus.NOT_FOUND, "Invalid role!"
                    ));
            roles.add(role);
        });

        user.setRoles(roles);
        userRepository.save(user);
    }
}
