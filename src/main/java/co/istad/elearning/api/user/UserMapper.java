package co.istad.elearning.api.user;

import co.istad.elearning.api.auth.dto.RegisterDto;
import co.istad.elearning.api.user.dto.UserCreationDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User fromUserCreationDto(UserCreationDto userCreationDto);

    UserCreationDto mapRegisterDtoToUserCreationDto(RegisterDto registerDto);

}
